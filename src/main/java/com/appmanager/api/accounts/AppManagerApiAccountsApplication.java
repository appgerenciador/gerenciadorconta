package com.appmanager.api.accounts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class AppManagerApiAccountsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppManagerApiAccountsApplication.class, args);
	}

}
